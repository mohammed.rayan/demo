const express = require('express');
const router = express.Router();
const { v4: uuidv4 } = require('uuid');
const _ = require('lodash');
const User = require('../lib/models/user');
const Account = require('../lib/models/account');
const UserAccount = require('../lib/models/userAccount');

const mailer = require('../lib/mailer');
const logger = require('../logger').logger('routes/user');

router.post('/activation', async (req, res) => {
    const email = req.body.email;
    const regexp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!regexp.test(String(email).trim().toLowerCase())) {
        logger.error(`Invalid email: ${email}`);
        return res.sendStatus(422);
    }

    const u = await User.findByEmail(email);
    // If not found
    if (_.isNull(u))
        return res.sendStatus(403);

    // Generate pin and save
    u.pin = Math.floor(10000000 + Math.random() * 90000000);
    u.create();

    // Notify the user
    mailer(email, 'pin', { pin: u.pin });
    logger.info(`Generated pin : ${u.pin}`);
    return res.sendStatus(200);
});

router.post('/auth', async (req, res) => {
    const email = req.body.email;
    const pin = req.body.pin;

    const u = await User.findByEmail(email);
    // If not found
    if (_.isNull(u))
        return res.sendStatus(401);


    // If PIN matches
    if (pin == u.pin) {
        u.token = uuidv4();
        u.create();

        logger.info('Generated token: ${u.token}');
        if(email !== "deepima.dwivedi@avaamo.com"){
            const userAcc = await UserAccount.findByUserId(u._id);
            return res.send({ accountId: userAcc.account,token: u.token, firstName: u.firstName, lastName: u.lastName, roles: userAcc.roles, filter: u.filter }).status(200);
        }else{
            return res.send({token: u.token }).status(200); 
        }
        
    } else
        return res.sendStatus(403);
});

router.post('/:id/add', async(req,res) => {
    const token = req.header('Access-Token');
    const user = await User.findByToken(token);
    
    // If user not found
    if (_.isNull(user))
        return res.sendStatus(401);

    const accountId = req.params.id;
    const account = await Account.find(accountId);

    if (_.isNull(account))
        return res.sendStatus(401);

    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const email = req.body.email;
    const admin = req.body.admin;
    const workHours = req.body.workHours;
    const timezone = req.body.timezone;
    const accountRole = req.body.roles;

    const u = new User();
    u.firstName = firstName;
    u.lastName = lastName;
    u.admin = admin;
    u.workHours = workHours;
    u.timezone = timezone;
    u.email = email;
    u.filter = req.body.filter;
    await u.create();

    const userAccount = new UserAccount();
    userAccount.account = accountId;
    userAccount.user = u;
    userAccount.roles = accountRole;
    await userAccount.create();

    return res.json(u).status(200);
});

router.put('/:id', async(req,res) => {
    const token = req.header('Access-Token');
    const user = await User.findByToken(token);
    
    // If user not found
    if (_.isNull(user))
        return res.sendStatus(401);

    const userid = req.params.id;
    
    let account = await User.updateOne({ _id : userid}, { $set: req.body }).exec();
    let userAccount = await UserAccount.findOneAndUpdate({ user : userid}, { $set: {
        roles: req.body.roles
    }}).exec();
    return res.json(account).status(200);
});

router.delete('/:id', async(req,res) => {
    const token = req.header('Access-Token');
    const user = await User.findByToken(token);
    
    // If user not found
    if (_.isNull(user))
        return res.sendStatus(401);

    const userid = req.params.id;
    
    let account = await User.deleteOne({ _id : userid}).exec();

    return res.json(account).status(200);
});

router.get('/:id', async(req,res) => {
    const token = req.header('Access-Token');
    const user = await User.findByToken(token);
    
    // If user not found
    if (_.isNull(user))
        return res.sendStatus(401);

    const userid = req.params.id;
    
    let account = await User.findById(userid);
    console.log(account);
    if(account == null) {
        return res.json({}).status(200);
    }
    return res.json(account).status(200);
});

router.get('/', async(req,res) => {
    const token = req.header('Access-Token');
    const user = await User.findByToken(token);
    
    // If user not found
    if (_.isNull(user))
        return res.sendStatus(401);

    const userid = req.params.id;
    
    let account = await User.findById(userid);
    console.log(account);
    if(account == null) {
        return res.json({}).status(200);
    }
    return res.json(account).status(200);
});

router.get('/token/:id', async(req,res) => {

    const user = await User.findByToken(id);
    if (_.isNull(user))
        return res.sendStatus(401);
        
    return res.json(user).status(200);
});

module.exports = router;
