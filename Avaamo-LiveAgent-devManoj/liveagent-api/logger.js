const _ = require('lodash');
const winston = require('winston');
const moment = require('moment-timezone');

// Get timestamp in specified time zone
const timestamp = winston.format((info, opts) => {
    if (opts.tz)
        info.timestamp = moment().tz(opts.tz).format();
    return info;
});
// Setting timestamp for India
const logFormat = winston.format.combine(
    winston.format.simple(),
    timestamp({ tz: 'Asia/Calcutta' }),
    winston.format.printf(info => `${info.timestamp} - ${info.service} - ${info.level} - ${info.message}`),
);

/**
 * @param {string} service  Name of the service which is writing the log
 */
const logger = (service) => {
    return winston.createLogger({
        format: logFormat,
        defaultMeta: { service: service },
        transports: [
            new winston.transports.File({ filename: './logs/app.log' }),
            new winston.transports.Console()
        ],
    });
};

class Logger {
    constructor() {
        this.logger = logger;
        this.middleware = (req, res, next) => {
            logger('app').info(`URL     : ${req.method} ${req.url}`);
            if (req.body && !_.isEmpty(req.body))
                logger('app').info(`Headers : ${JSON.stringify(req.headers)}`);
            if (req.body && !_.isEmpty(req.body))
                logger('app').info(`Body    : ${JSON.stringify(req.body)}`);

            next();
        };
    }
}

module.exports = new Logger();